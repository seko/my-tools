# Release Notes iSynth CLI

These Release Notes start with version 1.5.0 of the CLI.

		
### v1.12.0
- command `--run-order-config` now also supports async execution of remote order config files (i.e. order config files stored in the iSynth environment)
- command `--run-pytest` now passes through the responses of iSynth towards the caller
- compiled binaries for windows and linux are a little more optimized regarding size (roughly 50% smaller) (no optimizations yet for Mac OS binaries since these optimizations are not yet supported on Mac OS)
- upgrade all dependencies to the newest version

		
### v1.11.0
- print all flags and arguments of a CLI call if used with `--verbose`
- add support for single cli releases to `get-release-notes`
- add `--reset-only` option to `init` command if you just want to reset your environment database (e.g. for a clean restart of your tests) without re-generating the model
- upgrade all dependencies to the newest version

		
### v1.10.0 (iSynth 3.0)
- add support for iSynth 3.0
    - support the new structured responses of iSynth plugins (including --json-path argument)
    - support for the new workflow endpoints (.../`provisioning`/...). The previous endpoints (`deploy`) are still the default but marked as deprecated
    - add `get-isynth-version` command to get the version of the iSynth server
- add `--time-between-retries` to control how much time the CLI should wait before retrying a failed call (was 3 seconds before)
- add `--show-log` to the provision data command which will show the iSynth workflow logs
- add `get-release-notes` command which prints these release notes of iSynth CLI
- add `download-files` command which downloads content (files or folders) from the iSynth environment
- upgrade all dependencies to the newest version

		
### v1.9.0
- upgrade to Golang 1.21
- upgrade all dependencies to the newest version
- add status command for retrieving the status of an iSynth instance

		
### v1.8.2
- fix too verbose return value of the execute-plugin CLI command

		
### v1.8.1
- no functional change
- binaries are now compiled with CGO_ENABLED=0. This reduces a potential dependency to locally installed library. Avoids the following error:

```bash
./isynth-cli-v1.8.0-linux: /lib/x86_64-linux-gnu/libc.so.6: version 'GLIBC_2.32' not found (required by ./isynth-cli-v1.8.0-linux)
```

		
### v1.8.0
- **run-order** additionally allows to execute an order defined by a string or a local physical file (rather than requiring a physical file inside the iSynth environment). Requires iSynth version >= 2.7
- **run-order-config** additionally allows to execute an order config defined by a string or a local physical file (rather than requiring a physical file inside the iSynth environment). Requires iSynth version >= 2.7.
These new options allow asynchroneous execution of order configs (the _traditional_ way with a pre-defined order config within the iSynth environment still does not support asynchroneous execution)
- upgrade all dependencies to the newest version

		
### v1.7.2
- improved wording in CLI documentation

		
### v1.7.1
- improved documentation of the CLI (both inline (--help) as well as .md documents)
  - added examples to the various CLI commands
  - grouped the available commands in the documentation
  - grouped the flags for each command (mandatory, optional,...)

		
### v1.7.0
- add support for new iSynth 2.7 feature: run orders asynchroneously (helps against timeout issues)
- upgrade all dependencies to the newest version
- add documentation to --help about how to use https with custom certificates using environment variables

		
### v1.6.2
- upgrade all dependencies to the newest version
- add "--silent" flag to suppress all log outputs besides errors

		
### v1.6.1
- upgrade all dependencies to the newest version
- make sure to use new request objects for all calls

		
### v1.6.0
- handle HTTP/2 GOAWAY errors which might be retryable
- new flag --force-http-1 to enforce usage of HTTP/1.1 instead of HTTP/2
- iSynth CLI version information will be sent in HTTP request headers
- send Connection: keep-alive HTTP header

		
### v1.5.0
- new iSynth CLI command refresh-constellation-infos
